#Recodificación

recodificar<-function(datos_estudio, pregunta=""){
  if(pregunta==""){
    v_reversar<-c("P33", 
                  "P37", 
                  "P40", 
                  "P52", 
                  "P57", 
                  "P60", 
                  "P77", 
                  "P80", 
                  "P60", 
                  "P40" );
  }else{
    v_reversar<-c(pregunta)
  }
  
  columns_names=colnames(datos_estudio)
  
  for(columna in columns_names){
    
    if(is.element(columna,v_reversar))  {
      
      for(fila in c(1:nrow(datos_estudio))){
        
        datos_estudio[fila,columna]=8 - datos_estudio[fila,columna]
      }
      
    }
    
  }
  
  return(datos_estudio)
}